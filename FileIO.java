/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.StandardOpenOption;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;



/**
 *
 * @author ayrto
 */
public class FileIO {
    public static void main(String[] args){
        
        showOpenDialog(); //calling the function
    }
   
    public static String showSaveDialog(){ //making a new method which has a return statement as no void is present
        
        JFileChooser chooser = new JFileChooser();
        chooser.setSelectedFile(new File("fileToSave.txt")); //name suggested
        int returnVal = chooser.showSaveDialog(null);
        
        if(returnVal == JFileChooser.APPROVE_OPTION) //user did not click cancel
            
        {
            String chosenPath = chooser.getSelectedFile().getAbsolutePath();
            return chosenPath;
            //save file in path
        }
        else{
            String name = "cannot click cancel";
            return name;
            
        }
        
        
        
        
        
    }
    
    public static String showOpenDialog(){
         String openFile="";
        JFileChooser chooser = new JFileChooser();
        //setting file filter
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Text Files",
        "txt");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(chooser);
        
        if(returnVal == JFileChooser.APPROVE_OPTION){
             String chosen = chooser.getSelectedFile().getAbsolutePath();
                  openFile = chosen;
            //open file in chosen path
        }
        else
        {
            JOptionPane.showMessageDialog(null,"Please select a file");
            
        }
        return openFile;
    }
    
    public static String readTextFile(String filePath){
        
        String file="";
        Path path = Paths.get(filePath);
        

        try
        {
            BufferedReader reader = null;
            
            reader = Files.newBufferedReader(path,Charset.defaultCharset());
            
            String line = null;
            while((line = reader.readLine())!= null){
                file+=line;
            }
            reader.close();
            
        }
        catch (IOException io)
        {
             System.out.println("Problem while reading file!"); 
                    io.printStackTrace();
        }
        return file;
    }
    
    public static void writeToTextFile(String filePath, String toWrite){
        Path paths=Paths.get(filePath);
      try
      {
           BufferedWriter writer = Files.newBufferedWriter(paths, Charset.defaultCharset(),StandardOpenOption.CREATE);
           writer.write(toWrite);
           writer.close();
       }
      catch (IOException io)
        {
             JOptionPane.showMessageDialog(null,"problem while creating the file" + io);
        }
     
    }
    
    

    
    
}

